/** Created by Juan Manuel Ventura */

'use strict';

var fs = require('fs');
var path = require('path');
var debug = require('debug')('app:automounter');

/**
 * Recursively mount controllers on resourceful routes
 * @module Core
 * @param {Object} app The express app
 * @param {Function} app.use The express middleware function
 * @param {String} controllersFolder The controllers' folder of the express app
 */
module.exports.mount = function mountRoutes(app, controllersFolder) {

  buildRoutes(controllersFolder).sort(alphabetically).forEach(function (route) {
    app.use(route.path, require(route.handler));
    debug('mounted route %s', route.path);
  });

  function buildRoutes(folder, routes) {
    routes = routes || [];
    var folderPath = path.resolve(controllersFolder, folder);
    var rootFolder = folder.replace(controllersFolder, '') || '/';

    fs.readdirSync(folderPath).forEach(function (file) {
      if (fs.statSync(path.resolve(folderPath, file)).isDirectory()) return buildRoutes(path.join(folderPath, file), routes);

      if (path.extname(file) === '.js') {
        routes.push({
          path:    (file === 'index.js' ? rootFolder : path.join(rootFolder, path.basename(file, '.js'))).replace(' ', '-'),
          handler: path.resolve(folderPath, file)
        });
      }

    });

    return routes;
  }

};

function alphabetically(a, b) {
  return a.path < b.path ? -1 : 1;
}