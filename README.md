An express controllers automounter based on restfull resources

**Usage:**

```
#!javascript
var express = require('express');
var app =  express();
var mounter = require('routesMounter');

// must pass the app instance + the absolute path to the controllers folder
mounter.mount(app, path.resolve(__dirname, './app/controllers'));

```